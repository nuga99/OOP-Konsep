/* Example of Template Inheritance */

class Sepeda{
  // Kelas Bapak
  public int gear; // gear sepeda
  public int speed; //kecepatan
  
  // Konstruktor hanya 1
  
  public Sepeda (int gear, int speed){
    
    this.gear = gear;
    this.speed = speed;
  }
  
  // Void of decrement speed
  public void reduceSpeed(int decrement){
    
    speed-=decrement;
  }

  // Void of decrement speed
  public void moreSpeed(int increment){
    
    speed+=increment;
  }
  
  public String Kalimat(){
    return "----Program Sepeda Fixie dengan Inheritance----\n"+"\nGigi ke "+gear+"\n"+"Kecepatan sepeda "+speed;
  }
}
  
/* Mengaktifkan extends untuk menurunkan sifat dari Ayah */

class SepedaFixie extends Sepeda{
  
  public int heightSeat;
  public int sizeofTire;
  
  //Konstruktor hanya 1
  
  public SepedaFixie(int gear,int speed, int Set_Height){
    
    super(gear,speed); // Aktifasi dari turunan Ayah
    heightSeat = Set_Height;
  }
  
  public void Set_Height(int Value){
    
    heightSeat = Value;
  }
  
  // Melakukan Override
  @Override
  public String Kalimat(){
    return super.Kalimat()+ "\nTinggi tempat duduk "+heightSeat;
  }
}

class InheritanceExample {
  public static void main(String[] args) {
    
    
    SepedaFixie sepeda = new SepedaFixie(5,3,70);
    System.out.println(sepeda.Kalimat());
    
    
  }
}