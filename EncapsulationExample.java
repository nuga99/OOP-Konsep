
/* Example of Encapsulation in Java */

class Encapsulate{
  
  // Declare the private access variable
  private int getUmur;
  private int getTinggi;
  private String getNama;
  
  // Declare methode Get access as public variable
  public int getUmur() {return getUmur;}
  public int getTinggi(){return getTinggi;}
  public String getNama(){return getNama;}
  
  // Declare methode Set for each variable --> Private
  public void setUmur(int newUmur){ getUmur=newUmur;}
  public void setTinggi(int newTinggi){getTinggi=newTinggi;}
  public void setNama(String newNama){getNama=newNama;}
  
}


// Main Driver (Program yang dijalankan)

public class EncapsulationExample{
  public static void main(String[] args) {
  // Reference dari Class Encapsulate dengan nama variable "enc"
    Encapsulate enc = new Encapsulate();
    
    // Variable for testing
    enc.setNama("Nuga");
    enc.setUmur(19);
    enc.setTinggi(177);
    
    // Output
    System.out.println("Nama panggilan : "+enc.getNama());
    System.out.println("Umur : "+enc.getUmur());
    System.out.println("Tinggi Badan : "+enc.getTinggi()+" cm");

  }
}